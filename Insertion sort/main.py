import random
from matplotlib import pyplot  as plt

import time

def Insert(list):
 
    for i in range(1,len(list)):
        j = i - 1
        while (j >= 0 ):
            if list[j] > list[j+1]:
                aux =  list[j+1]
                list[j+1] = list[j]
                list[j] = aux
            j -=1

def calcTime():
    list = []
    len_list = [10,100,1000]
    tempos = []

    for size in len_list:
        for i in range(size,0,-1):
            list.append(i)

        time_s = time.time()
        Insert(list)
        time_e = time.time()
        tempos.append(time_e - time_s)

    plt.plot(len_list,tempos)
    plt.xlabel('interações')
    plt.ylabel('Tempo')
    plt.title('Insert sort em python')

    plt.show()

def main():
    calcTime()

if __name__ == '__main__':
    main()
