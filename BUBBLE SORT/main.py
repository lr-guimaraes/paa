import random
from matplotlib import pyplot  as plt

import time

def Bubble(list):

    for i in range(len(list)):
        for j in range(1,len(list)):
            if list[j-1] > list[j]:
                aux =  list[j-1]
                list[j-1] = list[j]
                list[j] = aux

def calcTime():
    list = []
    len_list = [10,100,1000,10000]
    tempos = []

    for size in len_list:
        for i in range(size,0,-1):
            list.append(i)

        time_s = time.time()
        Bubble(list)
        time_e = time.time()
        tempos.append(time_e - time_s)

    plt.plot(len_list,tempos)
    plt.xlabel('interações')
    plt.ylabel('Tempo')
    plt.title('Bubble sort em python')

    plt.show()

def main():
    calcTime()

if __name__ == '__main__':
    main()
