from matplotlib import pyplot  as plt
import time

def selection(list):
    index  = 0
    for i in range(len(list)):
        j = i
        while (j < len(list)):
            if list[j] > list[j-1]:
                index = j - 1
            j +=1
        aux = list[i]
        list[i] = list[index]
        list[index] = aux    


def calcTime():
    list = []
    len_list = [10,100,1000]
    tempos = []

    for size in len_list:
        for i in range(size,0,-1):
            list.append(i)

        time_s = time.time()
        selection(list)
        time_e = time.time()
        tempos.append(time_e - time_s)

    plt.plot(len_list,tempos)
    plt.xlabel('interações')
    plt.ylabel('Tempo')
    plt.title('selection sort em python')

    plt.show()

def main():
    calcTime()

if __name__ == '__main__':
    main()
